function injectGlobalContent(tag, type, ...contents) {
  var head = document.getElementsByTagName('head') [0];
  if (!head) return false;
  var elt = document.createElement(tag);
  elt.type = type;
  for (var i = 0; i < contents.length; i++) elt.innerHTML = elt.innerHTML + contents[i] + "\n";
  head.appendChild(elt);
  return true;
}
 
function addGlobalStyle(...css) {
  injectGlobalContent("style", "text/css", css);
}

function observeElement(elt, action, observedEvents = { attributes: true, childList: true }) {
  if (!elt) return false;
  var observer = new MutationObserver(action); if (!observer) return false;
  observer.observe(elt, observedEvents);
  return true;
}

function actionByElements(elts, act) {
  if (!elts) return 0;
  for (var i=0; i<elts.length; i++) act(elts[i]);
  return elts.length;
}
 
function actionByClassName(cls, act) {
  var elts = document.getElementsByClassName(cls);
  return actionByElements(elts, act);
}

function actionBySelector(sel, act) {
  var elts = document.querySelectorAll(sel);
  return actionByElements(elts, act);
}
 
function actionByID(id, act) {
  var elts = document.querySelectorAll("[id='" + id + "']");
  return actionByElements(elts, act);
}

function actionByAttribute(attname, attval, act) {
  var elts = document.querySelectorAll("[" + attname + "='" + attval + "']");
  return actionByElements(elts, act);
}

function repeatAction(action, duration = -1, every = 200, stopIfTrue = false ) {   // 'duration' and 'every' in milliseconds
  if ((duration == 0) || (every <= 0)) return false;
  var rpt = setInterval(function() {
    var res = action();
    if ((stopIfTrue === true) && (res === true)) clearInterval(rpt);
  }, every);
  if (duration > 0) setTimeout(function() { clearInterval(rpt); }, duration);
  return true;
}

function insist(selector, action, duration = 10000, every = 200) {
  repeatAction(function() { return 0< actionBySelector(selector, e=> { action(e); } ); }, duration, every, true);
}

function insistIf(selector, condition, action, duration = 10000, every = 200) {
  if (!condition) condition = function(e) { return true; };
  repeatAction(function() {
    var res = false;
    actionBySelector(selector, e=> { 
      if (condition(e)) {
        res = true;
        action(e);
      }
    } );
    return res;
  }, duration, every, true);
}

function setValue(id, value, trigger = false) {
  return actionByID(id, e => { e.value = value; if (trigger) triggerEvent(e, 'change'); });
}

function check(id, trigger = false) { return idCheck(id, trigger); }	// <== to be deprecated
function focus(id, trigger = false) { return idFocus(id, trigger); }	// <== to be deprecated
function click(id, trigger = false) { return idClick(id, trigger); }	// <== to be deprecated

function idCheck(id, trigger = false) {
  return actionByID(id, e => { elementCheck(e, trigger); });
}
 
function idFocus(id, trigger = false) {
  return actionByID(id, e => { elementFocus(e, trigger); });
}

function idClick(id, trigger = false) {
  return actionByID(id, e => { elementClick(e, trigger); });
}

function elementCheck(elt, trigger = false) { elt.click(); if (trigger) triggerEvent(elt, 'click'); }
function elementFocus(elt, trigger = false) { elt.focus(); if (trigger) triggerEvent(elt, 'focus'); }
function elementClick(elt, trigger = false) { elt.click(); if (trigger) triggerEvent(elt, 'click'); }

function setAttribute(id, attrname, attrvalue) {
  return actionByID(id, e => { e.setAttribute(attrname, attrvalue); });
}

function remove(e) {
  if ((!e) || (!e.parentElement)) return false;
  e.parentElement.removeChild(e);
  return true;
}

function hide(e) {
  if (!e) return false;
  e.style.display = 'none';
  return true;
}

// Example: triggerEvent(e, 'change');
function triggerEvent(elt, evtType) {
  // IE9+ and other modern browsers
  if ('createEvent' in document) {
    var e = document.createEvent('HTMLEvents');
    e.initEvent(evtType, false, true);
    elt.dispatchEvent(e);
  } else {
    // IE8
    var e = document.createEventObject();
    e.eventType = evtType;
    elt.fireEvent('on' + e.eventType, e);
  }
}

window.requestAnimationFrame = (function() {
  return window.requestAnimationFrame ||
     window.webkitRequestAnimationFrame ||
     window.mozRequestAnimationFrame ||
     window.oRequestAnimationFrame ||
     window.msRequestAnimationFrame ||
     function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
       window.setTimeout(callback, 1000/60);
     };
})();

// "&#9776; label" to display the burger symbol before the label
function wrapBloc(label, e, expanded=true) {
  let i = (""+Math.random()).replace("0.","wrapBloc").trim();
  e.outerHTML = `<span onclick="javascript: var e=document.getElementById('${i}'); e.style.display=(e.style.display==='block'?'none':'block');">${label}</span><div id='${i}' style='display: ${expanded?"block":"none"};'>`
  + e.outerHTML
  + "</div>";
}

// Exemple de positionStyle : "position: fixed; top: 50px; left: 0px; display: block;"
// Exemple de maxHeight : 450
// Exemple de srcIgnoreCondition : "src.includes('/images/avatars/avatar') || src.includes('data:image/png;base64,')"
// Exemple de srcReplace : "src.replace('.jpg.44x44.jpg', '.jpg').replace('.jpeg.44x44.jpg', '.jpeg').replace('.44x44.jpg', '.full.jpg')"
function plugImageViewer(positionStyle, maxHeight, srcIgnoreCondition, srcReplace) {
  if (( ! srcIgnoreCondition) || (srcIgnoreCondition === "")) srcIgnoreCondition = "false";
  if (( ! srcReplace) || (srcReplace === "")) srcReplace = "src";
  injectGlobalContent("script", "text/javascript",
                      "var previmg = '';",
                      "var wi = new Image();",
                      "wi.src = 'https://framagit.org/etacarina/pub/-/raw/main/monkey/ajax-loader.gif';",
                      "function showFullImg(src) {",
                      "  if (previmg == src) return;",
                      "  if (" + srcIgnoreCondition + ") return;",    // à paramétrer
                      "  var elt = document.getElementById('fulimgdiv');",
                      "  if (!elt) {",
                      "    var bdy = document.getElementsByTagName('body')[0];",
                      "    if (bdy) {",
                      "      elt = document.createElement('div');",
                      "      elt.setAttribute('id', 'fulimgdiv');",
                      "      elt.setAttribute('onclick', 'hideFullImg()');",
                      "      elt.style.zIndex = '999';",
                      "      elt.innerHTML = \"<img id=\'fulimg\' height=\'\" + wi.height + \"px\' src=\'\" + wi.src + \"\'>\";",
                      "      bdy.appendChild(elt);",
                      "    }",
                      "  }",
                      "  if (elt) {",
                      "    previmg = src;",
                      "    img = document.getElementById('fulimg');",
                      "    if (img) {",
                      "      img.height = wi.height;",
                      "      img.src = wi.src;",
                      "      elt.style = '" + positionStyle + "';",
                      "      var hei = (('innerHeight' in window) ? window.innerHeight : document.documentElement.offsetHeight);",
                      "      hei = Math.floor(Math.max(0.8*hei, " + maxHeight + "))",
                      "      var bufimg = new Image();",
                      "      bufimg.name = src;",
                      "      bufimg.onload = function(){img.src = this.src; img.height = Math.min(hei, this.height);};",
                      "      bufimg.src = " + srcReplace + ";",
                      "    }",
                      "  }",
                      "}",
                      "function hideFullImg() {",
                      "  var elt = document.getElementById('fulimgdiv');",
                      "  if (elt) {",
                      "    elt.style = 'display: none;';",
                      "    var img = document.getElementById('fulimg');",
                      "    if (img) {",
                      "      img.height = wi.height;",
                      "      img.src = wi.src;",
                      "    }",
                      "  }",
                      "}",
                     );
}

function triggerImageViewer(cls, elementModifier) {
  actionByClassName(cls, function(x) {
    if (typeof elementModifier !== "undefined") x = elementModifier(x);
    if (!x) return;
    var imgsrc = x.getAttribute('src');
    if (!imgsrc) return;
    var imgmax = maxImage(imgsrc);
    // alert(imgsrc + ' => ' +imgmax);
    if (!x.getAttribute("onmouseover")) x.setAttribute("onmouseover", "showFullImg('" + imgmax + "')");
  });
}

function maxImage(src) {
  if (src.includes('.ggpht.com/')) return src.split("=")[0];
  if (src.includes('.ytimg.com/')) return src.split("?")[0];
  if (src.startsWith('https://cdn-images-') && src.includes('.medium.com/')) return src.replace('/fit/c/41/41/', '/');
  if (src.includes('.cloudfront.net/user_photos/')) return src.replace('56x56', '44x44').replace('46x46', '44x44').replace('76x76', '44x44').replace('.jpg.44x44.jpg', '.jpg').replace('.jpeg.44x44.jpg', '.jpeg').replace('.jpeg.44x44.jpeg', '.jpeg').replace('.png.44x44.png', '.png').replace('.44x44.jpg', '.full.jpg');
  if (src.startsWith('https://www.gravatar.com/')) return src.replace('?s=32', '?s=256').replace('?s=96', '?s=256');
  if (src.includes('.imgur.com/')) return src.replace('?s=32&g=1', '').replace('?s=96&g=1', '');
  if (src.startsWith('https://jira.azfr.allianz/secure/useravatar')) return src.replace('size=xsmall', 'size=xxlarge');
  if (src.startsWith('https://avatars') && src.includes('.githubusercontent.com/')) return src.replace('s=60&', '');
  return src;
}
