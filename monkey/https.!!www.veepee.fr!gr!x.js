addGlobalStyle(`
  .productItem { border: 4px solid #e1e1e4 !important; border-radius: 20px !important; }
  .modalHook { background: #DDD !important; }
  header[class^="header_"] { height: 70px !important; }
  a[href="/gr/sponsorship"] { display:none !important; }
  nav[class^="header_"], img[class*="header_"], div[class^="header_"] { height: 34px !important; grid-template-rows: unset !important; }
  div[class^="styles__BackgroundTheme-groot_"] { background-image: unset !important; background-color: dimgray !important; }
  // div[class*="styles_"] { background: dimgray !important; }
`);

if (window.location.pathname.startsWith("/gr/home")) {
  var rpt = setInterval(function() {
    
    var cnt = actionBySelector("a[aria-label='Banner link']", e => { e.setAttribute("target", "_blank"); })
            + actionBySelector("div[class*='styles__Wrapper-groot'] a", e => { e.setAttribute("target", "_blank"); });
    if (0< cnt) console.log(cnt + ' link targets set to _blank');

    // let bgcolor = "dimgray";
    // actionBySelector('div[class*="styles__Wrapper-groot"]', e => { e.style.backgroundColor = bgcolor; });
    // actionBySelector('div[class*="styles__BackgroundTheme-groot"]', e => { e.style.background = "none"; });
    // actionBySelector('div[class*="styles__Gradient-groot"]', e => { e.style.backgroundColor = bgcolor; });
  }, 2000);
}

if (window.location.pathname.startsWith("/gr/catalog/")) {
  var rpt = setInterval(function() {
    var cnt = actionBySelector("div[class*='styles__ItemColumn-groot'] a", e => { e.setAttribute("target", "_blank"); });
    if (0< cnt) console.log(cnt + ' link targets set to _blank');
    
    actionBySelector("div[class*='styles__ItemColumn-groot']", e => { e.style.border="2px solid gainsboro"; });
    
  }, 2000);
}

repeatAction(function() {
  console.log(window.location.pathname + "   " + window.location.pathname.startsWith("/gr/product/") );
  if ( ! window.location.pathname.startsWith("/gr/product/")) return false;
  console.log("check show more");
  if (0< actionBySelector("button[class*='styles__ShowMore-groot']", e=> { console.log("click show more"); elementClick(e, true); }) ) return true;
  actionBySelector('iframe', f=> {
    var elts = f.contentWindow.document.body.getElementsByClassName('showMore');
    for (var i=0; i<elts.length; i++) elementClick(elts[i], true);
  });
  
  // return (0< actionBySelector('button[class*="styles__ShowMore-groot"]', e=> { elementClick(e, true); })
  //          +actionBySelector('a.showMore', e=> { console.log("click show more"); elementClick(e, true); }));
}, -1, 1000, true);
/*
if (window.location.pathname.includes("/productsheet/")) {
  repeatAction(function() {
    return (0< actionBySelector('button[class*="styles__ShowMore-groot"]', e=> { elementClick(e, true); })
              +actionBySelector('a.showMore', e=> { elementClick(e, true); }));
  }, -1, 1000, false);
}*/

setTimeout(function() {
  actionBySelector("body", bdy => {
    observeElement(bdy, function() {
      if (0< actionBySelector("div", d => { if (d.textContent === "Se connecter") elementClick(d, true); }) )
        console.log('Click "Se connecter"');
      if (0< actionByClassName("showMore", e => { elementClick(e, true); }) )
        console.log('Click "Lire la suite"');
    });
  });
}, 2000);

