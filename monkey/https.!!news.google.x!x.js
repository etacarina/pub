addGlobalStyle(`
h4 {
  display: block !important;
  overflow: unset !important;
  color: mediumblue !important;
  font-size: medium !important;
}
img {
  border-radius: unset !important;
}
`);

repeatAction(
  function() {
    for (let elts = document.querySelectorAll('a[target="_blank"]'), i=0; i<elts.length; i++)  elts[i].style.cssText = "display: unset !important;";  
  }
, duration = -1, every = 1000, stopIfTrue = false );