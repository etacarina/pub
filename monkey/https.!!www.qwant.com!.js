addGlobalStyle(`
  a.external { display: block !important; }
`);

window.history.replaceState(null, '', window.location.href.replace('&t=web', ''));

repeatAction(function() {
  console.log("clear ads");
  actionBySelector('div.result--ext[data-monitoring^="ads"]', e => { hide(e); });
  actionByClassName('is-sidebar', e => { remove(e); } );
}, 2000, 200, true);
