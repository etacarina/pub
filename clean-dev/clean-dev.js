javascript:(function(){
    function doit(s, a) {var e=document.querySelectorAll(s); for (let i of e) a(i); return e.length;}
    function kill(e) {e.parentNode.removeChild(e);}
    function killsel(s) {return doit(s, e=>{kill(e);});}
    function rmcls(c) {return doit("."+c, e=>{e.classList.remove(c);});}
    for (let c of
    "didomi-popup-open blurText appconsent_noscroll modal-open sp-message-open noscroll"
    .split(" ")) rmcls(c);
    doit("[style*='overflow']", e=>{e.style.overflow="scroll";});
    killsel("div.truste_box_overlay, div.truste_overlay, div.piano-paywall");
    if (killsel(
    "#didomi-host, div[data-cbh='consent.google.com'], didomi-popup, #artdeco-global-alert-container, .snigel-cmp-framework, [id*='Cookiebot'], div[data-testid='cookie-policy-dialog'], .message--register"
    )) return;
    doit("body *", e=>{if (getComputedStyle(e).position==='fixed') kill(e);});
    }());
})
